class Component {
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
		if (this.children == null) {
			this.children = null;
		}
		if (this.attribute == null) {
			this.attribute = null;
		}
	}

	render() {
		if (this.attribute != null) {
			return this.renderAttribute();
		} else if (this.children == '') {
			return `<${this.tagName} />`;
		} else {
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		}
	}

	renderAttribute() {
		if (this.children == null) {
			return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"/>`;
		} else {
			return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}">${this.children}</${this.tagName}>`;
		}
	}

	renderChildren() {
		if (this.attribute != null) {
			return this.renderAttribute();
		} else if (this.children instanceof Array) {
			return `<${this.tagName}>` + this.aff() + `</${this.tagName}>`;
		} else if (this.children == '') {
			return `<${this.tagName} />`;
		} else {
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		}
	}

	aff() {
		let s = '';
		this.children.forEach(element => {
			s += element;
		});
		return s;
	}
}
export default Component;
